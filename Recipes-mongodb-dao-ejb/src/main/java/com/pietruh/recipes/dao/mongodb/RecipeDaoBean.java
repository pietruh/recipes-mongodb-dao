package com.pietruh.recipes.dao.mongodb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PreDestroy;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.pietruh.recipes.dao.api.factory.ModelFactory;
import com.pietruh.recipes.dao.api.interfaces.RecipeDao;
import com.pietruh.recipes.dao.api.model.CuisineCategory;
import com.pietruh.recipes.dao.api.model.Description;
import com.pietruh.recipes.dao.api.model.Ingredient;
import com.pietruh.recipes.dao.api.model.Recipe;
import com.pietruh.recipes.dao.api.model.Weight;
import com.pietruh.recipes.dao.mongodb.interceptors.LoggerInterceptor;

@Stateless
@Remote(RecipeDao.class)
public class RecipeDaoBean implements RecipeDao {

	private static final String COLLECTION_NAME = "recipes";
	private static final String DB_NAME = "recipes";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RecipeDaoBean.class);

	// @Inject
	private ModelFactory modelFactory;

	private MongoClient mongoClient;

	@Inject
	public RecipeDaoBean(MongoClient mongoClient, ModelFactory modelFactory) {
		this.mongoClient = mongoClient;
		this.modelFactory = modelFactory;
	}

	@PreDestroy
	private void destroy() {
		this.mongoClient.close();
	}

	@Override
	@Interceptors(LoggerInterceptor.class)
	public List<Recipe> getRecipes() {
		List<Recipe> recipes = new ArrayList<>();
		DBCollection collection = getRecipesCollection();
		DBCursor cursor = collection.find();
		while (cursor.hasNext()) {
			Recipe recipe = getRecipe(cursor);
			recipes.add(recipe);
		}
		return recipes;
	}

	@Override
	@Interceptors(LoggerInterceptor.class)
	public List<Recipe> getUserRecipes(int userId) {
		List<Recipe> recipes = new ArrayList<>();
		try {
			DBCollection collection = getRecipesCollection();

			DBCursor cursor = collection.find();
			while (cursor.hasNext()) {
				Recipe recipe = getRecipe(cursor);
				recipes.add(recipe);
			}
		} finally {
			mongoClient.close();
		}
		return recipes;
	}

	@Override
	@Interceptors(LoggerInterceptor.class)
	public Recipe getRecipe(String recipeId) {
		try {
			DBCollection collection = getRecipesCollection();
			ObjectId id = new ObjectId(recipeId);

			BasicDBObject recipeQuery = new BasicDBObject();
			recipeQuery.put("_id", id);
			DBCursor cursor = collection.find(recipeQuery);
			while (cursor.hasNext()) {
				return this.getRecipe(cursor);
			}
		} finally {
			mongoClient.close();
		}
		return null;
	}

	@Override
	@Interceptors(LoggerInterceptor.class)
	public List<Recipe> getUserFavouriteRecipes(int userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Interceptors(LoggerInterceptor.class)
	public Recipe addRecipe(Recipe recipe) {
		try {
			DBCollection collection = getRecipesCollection();

			BasicDBObject document = this.createRecipe(recipe);
			collection.insert(document);
			recipe.setId(document.get(Recipe.ID).toString());
		} finally {
			mongoClient.close();
		}
		return recipe;
	}

	@Override
	@Interceptors(LoggerInterceptor.class)
	public void updateRecipe(Recipe recipe) {
		try {
			DBCollection collection = getRecipesCollection();

			BasicDBObject updateRecipe = new BasicDBObject();
			updateRecipe.append("$set", updateRecipe);

			BasicDBObject searchRecipe = new BasicDBObject();
			searchRecipe.put("_id", recipe.getId());

			collection.update(searchRecipe, updateRecipe);
		} finally {
			mongoClient.close();
		}
	}

	@Override
	@Interceptors(LoggerInterceptor.class)
	public void deleteRecipe(String recipeId) {
		try {
			DBCollection collection = getRecipesCollection();

			ObjectId objectId = new ObjectId(recipeId);
			BasicDBObject recipe = new BasicDBObject();
			recipe.put("_id", objectId);
			collection.remove(recipe);
		} finally {
			mongoClient.close();
		}
	}

	private DBCollection getRecipesCollection() {
		DB db = mongoClient.getDB(DB_NAME);
		return db.getCollection(COLLECTION_NAME);
	}

	private Recipe getRecipe(DBCursor cursor) {
		Recipe recipe = modelFactory.createRecipe();
		DBObject r = cursor.next();
		recipe.setId(((ObjectId) r.get(Recipe.ID)).toString());
		recipe.setCookingTime((int) r.get(Recipe.COOKING_TIME));
		recipe.setName((String) r.get(Recipe.NAME));
		BasicDBList ingredients = (BasicDBList) r.get(Recipe.INGREDIENTS);
		recipe.setIngredients(this.getIngredients(ingredients));
		BasicDBList descriptions = (BasicDBList) r.get(Recipe.DESCRIPTIONS);
		recipe.setDescriptions(this.getDescriptions(descriptions));
		BasicDBList cuisineCategory = (BasicDBList) r.get(Recipe.CUISINES);
		recipe.setCuisines(this.getCuisines(cuisineCategory));

		recipe.setPhoto((byte[]) r.get(Recipe.PHOTO));
		return recipe;
	}

	private List<CuisineCategory> getCuisines(BasicDBList list) {
		List<CuisineCategory> cuisineCategory = new ArrayList<>();
		DBObject[] lists = list.toArray(new DBObject[0]);

		for (DBObject object : Arrays.asList(lists)) {
			cuisineCategory.add(CuisineCategory.getById((int) object
					.get(Recipe.CuisineCategoryMongoColumnName.CUISINE_ID)));
		}
		return cuisineCategory;
	}

	private List<Ingredient> getIngredients(BasicDBList list) {
		List<Ingredient> descriptions = new ArrayList<>();
		DBObject[] lists = list.toArray(new DBObject[0]);

		for (DBObject object : Arrays.asList(lists)) {
			Ingredient ingredient = modelFactory.creatIngridient();
			ingredient.setName((String) object
					.get(Recipe.IngredientsMongoColumnName.NAME));
			ingredient.setAmount((int) object
					.get(Recipe.IngredientsMongoColumnName.AMOUNT));
			ingredient.setWeight(Weight.getByString((int) object
					.get(Recipe.IngredientsMongoColumnName.WEIGHT)));
			descriptions.add(ingredient);
		}
		return descriptions;
	}

	private List<Description> getDescriptions(BasicDBList list) {
		List<Description> descriptions = new ArrayList<>();
		DBObject[] lists = list.toArray(new DBObject[0]);

		for (DBObject object : Arrays.asList(lists)) {
			Description description = modelFactory.createDescription();
			description.setText((String) object
					.get(Recipe.DescriptionMongoColumnName.TEXT));
			description.setPhoto((byte[]) object
					.get(Recipe.DescriptionMongoColumnName.PHOTO));
			descriptions.add(description);
		}
		return descriptions;
	}

	private BasicDBObject createRecipe(Recipe recipe) {
		BasicDBObject document = new BasicDBObject();
		document.put(Recipe.NAME, recipe.getName());
		document.put(Recipe.COOKING_TIME, recipe.getCookingTime());
		document.put(Recipe.DESCRIPTIONS, this.createDescription(recipe));
		document.put(Recipe.INGREDIENTS, this.createIngredients(recipe));
		document.put(Recipe.CUISINES, this.createCuisines(recipe));
		document.put(Recipe.CREATE_DATE, new Date());
		document.put(Recipe.PHOTO, recipe.getPhoto());
		return document;
	}

	private BasicDBList createCuisines(Recipe recipe) {
		BasicDBList list = new BasicDBList();
		for (CuisineCategory category : recipe.getCuisines()) {
			BasicDBObject dbObject = new BasicDBObject();
			dbObject.put(Recipe.CuisineCategoryMongoColumnName.CUISINE_ID,
					category.getId());
			list.add(dbObject);
		}
		return list;
	}

	private BasicDBList createIngredients(Recipe recipe) {
		BasicDBList list = new BasicDBList();
		for (Ingredient ingr : recipe.getIngredients()) {
			BasicDBObject dbObject = new BasicDBObject();
			dbObject.put(Recipe.IngredientsMongoColumnName.NAME, ingr.getName());
			dbObject.put(Recipe.IngredientsMongoColumnName.AMOUNT,
					ingr.getAmount());
			dbObject.put(Recipe.IngredientsMongoColumnName.WEIGHT,
					Weight.getWeightId(ingr.getWeight()));
			list.add(dbObject);
		}
		return list;
	}

	private BasicDBList createDescription(Recipe recipe) {
		BasicDBList list = new BasicDBList();
		for (com.pietruh.recipes.dao.api.model.Description desc : recipe
				.getDescriptions()) {
			BasicDBObject dbObject = new BasicDBObject();
			dbObject.put(Recipe.DescriptionMongoColumnName.TEXT, desc.getText());
			dbObject.put(Recipe.DescriptionMongoColumnName.PHOTO,
					desc.getPhoto());
			list.add(dbObject);
		}
		return list;
	}
}
