package com.pietruh.recipes.dao.mongodb.exceptions;

public class MongoDbException extends Throwable {

	private static final long serialVersionUID = -1786030373901771811L;

	public MongoDbException() {
		super();
	}

	public MongoDbException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MongoDbException(String message, Throwable cause) {
		super(message, cause);
	}

	public MongoDbException(String message) {
		super(message);
	}

	public MongoDbException(Throwable cause) {
		super(cause);
	}
	
}
