package com.pietruh.recipes.dao.mongodb.interceptors;

import java.lang.reflect.Method;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerInterceptor {

	@AroundInvoke
	public Object log(InvocationContext ctx) throws Exception {
		String className = ctx.getTarget().getClass().getName();
		Method method = ctx.getMethod();
		Logger log = LoggerFactory.getLogger(className);

		log.debug(String.format("Start %s", method));
		Object ans = null;
		try {
			ans = ctx.proceed();
		} catch (Exception e) {
			log.error(String.format("Error while executing %s", method), e);
			throw e;
		}
		log.debug(String.format("End %s", method));

		return ans;
	}
}
