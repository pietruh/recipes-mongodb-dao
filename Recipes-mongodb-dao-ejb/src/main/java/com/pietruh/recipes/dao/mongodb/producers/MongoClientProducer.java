package com.pietruh.recipes.dao.mongodb.producers;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.pietruh.recipes.dao.mongodb.api.MongoConfiguration;
import com.pietruh.recipes.dao.mongodb.utils.MongoConfigurationUtils;

public class MongoClientProducer {

	private static final Logger LOGGER = LoggerFactory.getLogger(MongoClientProducer.class);

	private static final String HOST = "192.168.0.15";
	private static final int PORT = 27017;
	private static final String PASSWORD = "qwerty1";
	private static final String USERNAME = "recipeAdmin";
	private static final String DBNAME = "recipes";

	@RequestScoped
	@Produces
	public MongoClient createMongoClient() throws UnknownHostException {
		try {
			MongoConfiguration configuration = MongoConfigurationUtils
					.getDefaultConfigurationFromFile();
			return createMongoClient(configuration);
		} catch (UnknownHostException e) {
			throw e;
			// TODO: handle exception
		} catch (IOException e) {
			MongoConfiguration configuration = MongoConfiguration.builder().host(HOST).port(PORT)
					.username(USERNAME).passord(PASSWORD).build();
			return createMongoClient(configuration);
		} catch (Exception e) {
			LOGGER.error(
					String.format("Error while creating MongoClient HOST=%s, PORT=%d", HOST, PORT),
					e);
			throw new UnknownHostException(String.format(
					"Error while creating MongoClient HOST=%s, PORT=%d", HOST, PORT));
		}
	}

	private MongoClient createMongoClient(MongoConfiguration configuration)
			throws UnknownHostException {
		ServerAddress serverAddress = new ServerAddress(HOST, PORT);
		List<ServerAddress> addresses = new ArrayList<>();
		addresses.add(serverAddress);
		MongoCredential mongoCredential = MongoCredential.createMongoCRCredential(
				configuration.username(), DBNAME, PASSWORD.toCharArray());
		List<MongoCredential> credentials = new ArrayList<>();
		credentials.add(mongoCredential);

		return new MongoClient(addresses, credentials);
	}

}
