package com.pietruh.recipes.dao.mongodb.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.pietruh.recipes.dao.mongodb.api.MongoConfiguration;

public class MongoConfigurationUtils {

	public static MongoConfiguration getDefaultConfigurationFromFile() throws IOException {
		final String configFile = "./src/main/resources/mongodb.properties";
		return getMongoConfiguration(configFile);
	}

	public static MongoConfiguration getConfigurationFromFile(String configFile) throws IOException {
		return getMongoConfiguration(configFile);
	}

	private static MongoConfiguration getMongoConfiguration(final String name) throws IOException {
		InputStream input = null;
		try {
			System.out.println(System.getProperty("user.dir"));
			Properties prop = new Properties();
			input = new FileInputStream(name);
			// load a properties file
			prop.load(input);

			// get the property value and print it out
			String host = prop.getProperty("host");
			String port = prop.getProperty("port");

			String username = prop.getProperty("username");
			String password = prop.getProperty("password");

			return MongoConfiguration.builder().host(host).username(username).passord(password)
					.port(Integer.parseInt(port)).build();
		} catch (IOException e) {
			throw e;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
