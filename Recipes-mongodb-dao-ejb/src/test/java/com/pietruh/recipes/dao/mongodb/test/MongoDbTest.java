package com.pietruh.recipes.dao.mongodb.test;

import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.bson.types.Binary;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.pietruh.recipes.dao.mongodb.api.MongoConfiguration;
import com.pietruh.recipes.dao.mongodb.producers.MongoClientProducer;
import com.pietruh.recipes.dao.mongodb.utils.MongoConfigurationUtils;

public class MongoDbTest {

	// private MongoFactory factory;
	private MongoClient client = null;
	private DBCollection coll;
	private String dbName = "test";

	@Before
	public void before() throws Exception {
		client = new MongoClientProducer().createMongoClient();
		DB testDB = client.getDB(dbName);
		coll = testDB.getCollection("images");
		coll.insert(new BasicDBObject().append("image", "test"));
	}

	@Test
	public void testMongoDbTest() throws Exception {
		DB db = client.getDB(dbName);
		assertNotNull(db);

	}

	@Test
	public void propertiesTest() throws Exception {
		MongoConfiguration props = MongoConfigurationUtils.getDefaultConfigurationFromFile();
		assertNotNull(props);
	}

	@Test
	public void testImageInsert() throws Exception {
		String filename = "/home/pietruh/test.jpg";
		String empname = "ABC";

		/**
		 * Inserts a record with name = empname and photo specified by the
		 * filepath
		 **/
		insert(empname, filename, coll);
		// assertNotNull();

		String destfilename = "/home/pietruh/destfile.jpg";
		/**
		 * Retrieves record where name = empname, including his photo. Retrieved
		 * photo is stored at location filename
		 **/
		retrieve(empname, destfilename, coll);
	}

	@After
	public void after() throws Exception {
		// client.dropDatabase(dbName);
	}

	void insert(String empname, String filename, DBCollection collection) {
		try {
			File imageFile = new File(filename);
			FileInputStream f = new FileInputStream(imageFile);

			byte b[] = new byte[f.available()];
			f.read(b);

			Binary data = new Binary(b);
			BasicDBObject o = new BasicDBObject();
			o.append("name", empname).append("photo", data);
			collection.insert(o);
			System.out.println("Inserted record.");

			f.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void retrieve(String name, String filename, DBCollection collection) {
		byte c[];
		try {
			DBObject obj = collection.findOne(new BasicDBObject("name", name));
			String n = (String) obj.get("name");
			c = (byte[]) obj.get("photo");
			FileOutputStream fout = new FileOutputStream(filename);
			fout.write(c);
			fout.flush();
			System.out.println("Photo of " + name + " retrieved and stored at " + filename);
			fout.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
