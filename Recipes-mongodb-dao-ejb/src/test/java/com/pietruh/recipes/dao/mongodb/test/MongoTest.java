package com.pietruh.recipes.dao.mongodb.test;

import java.net.UnknownHostException;

import org.junit.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.pietruh.recipes.dao.mongodb.producers.MongoClientProducer;

public class MongoTest {
	@Test
	public void testName() throws Exception {
		MongoClient client = null;
		try {
			client =new  MongoClientProducer().createMongoClient();
			// MongoCredential credential=MongoCredential.
			DB db = client.getDB("recipes");
			// db.
			System.out.println("Collection: recipes");
			DBCollection collection = db.getCollection("recipes");
			System.out.println("\tcount:" + collection.getCount());

			// for (int i = 0; i < 1000000; i++) {
			//
			// }
			// BasicDBObject document = new BasicDBObject();
			// document.put("name", "pietruh");
			// document.put("age", 28);
			// document.put("createdDate", new Date());
			// collection.insert(document);

			// BasicDBObject document = new BasicDBObject();
			// document.put("name", "pietruh");
			// collection.remove(document);

			// System.out.println("\tcount:" + collection.getCount());
			//
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("name", "pietruh");

			DBCursor cursor = collection.find(searchQuery);
			while (cursor.hasNext()) {
				DBObject object = cursor.next();
				System.out.println(object);

				System.out.println("cursorId = " + object.get("_id"));
				for (String s : object.keySet()) {
					System.out.println("key=" + s);
				}

			}

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// return client;
	}
}
