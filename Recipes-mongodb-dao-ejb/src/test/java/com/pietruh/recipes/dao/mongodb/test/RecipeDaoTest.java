package com.pietruh.recipes.dao.mongodb.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;

import com.pietruh.recipes.dao.api.interfaces.RecipeDao;
import com.pietruh.recipes.dao.api.model.Recipe;
import com.pietruh.recipes.dao.mongodb.RecipeDaoBean;

@RunWith(MockitoJUnitRunner.class)
public class RecipeDaoTest {
	@InjectMocks
	private RecipeDaoBean recipeDao;

	@Test
	public void createRecipDao() throws Exception {
		// dao = Mockito.mock(RecipeDao.class);
		Recipe recipe = Mockito.mock(Recipe.class);
		recipeDao.addRecipe(recipe);
		Recipe recipe2 = recipeDao.getRecipe(recipe.getId());

		Mockito.verify(recipeDao).addRecipe(recipe);
//		org.mockito.Mockito
//		Matchers
		OngoingStubbing<Recipe> thenReturn = Mockito.when(recipeDao.getRecipe(recipe.getId())).thenReturn(recipe);

		assertEquals(recipe, thenReturn.getMock());
	}
}
