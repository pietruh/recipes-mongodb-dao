package com.pietruh.recipes.dao.mongodb.test;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.pietruh.recipes.dao.api.impl.model.DefaultRecipe;
import com.pietruh.recipes.dao.api.model.Recipe;
import com.pietruh.recipes.dao.mongodb.producers.MongoClientProducer;

public class RecipeTest {

	private MongoClient client = null;
	private DBCollection coll;
	private static final String COLLECTION_NAME = "recipes";
	private static final String DB_NAME = "recipes";

	@Before
	public void before() throws Exception {
		client = new MongoClientProducer().createMongoClient();
		client.dropDatabase(DB_NAME);
		DB testDB = client.getDB(DB_NAME);
		coll = testDB.getCollection(COLLECTION_NAME);
	}

	@Test
	public void testMongoDbTest() throws Exception {
		Recipe recipe = new DefaultRecipe();
		for (int i = 1; i < 200; i++) {
			recipe.setCookingTime(120);
			recipe.setName("Recipe " + i);
			BasicDBObject document = this.createRecipe(recipe);
			coll.insert(document);
		}

	}

	private BasicDBObject createRecipe(Recipe recipe) {
		BasicDBObject document = new BasicDBObject();
		document.put("name", recipe.getName());
		document.put("cookingTime", recipe.getCookingTime());
		document.put("ingridients", recipe.getIngredients());
		document.put("createdDate", new Date());
		document.put("photo", recipe.getPhoto());
		return document;
	}

}
