package com.pietruh.recipes.dao.mongodb.test;

public class Singleton {

	private static Singleton instance = new Singleton();

	private Object object1;
	private Object object2;
	private Object object3;

	private Singleton() {
		object1 = new Object();
		object2 = new Object();
		object3 = new Object();

	}

	public static Singleton getInstance() {
		return instance;
	}

	public Object getObject1() {
		return object1;
	}

	public void setObject1(Object object1) {
		this.object1 = object1;
	}

	public Object getObject2() {
		return object2;
	}

	public void setObject2(Object object2) {
		this.object2 = object2;
	}

	public Object getObject3() {
		return object3;
	}

	public void setObject3(Object object3) {
		this.object3 = object3;
	}

	public int doX() {
		// TODO Auto-generated method stub
		return 0;
	}

}